from django.utils import timezone
from rest_framework import serializers
from rest_framework.reverse import reverse
from rest_framework.serializers import ValidationError

from . import models


class SiswaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MasterSiswa
        fields = '__all__'


class TagihanSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MasterTagihan
        fields = '__all__'


class DiskonSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MasterDiskon
        fields = '__all__'

class PelunasanTagihanSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.PelunasanTagihan
        fields = '__all__'

    siswa = serializers.SerializerMethodField()
    diskon = serializers.SerializerMethodField()
    kelas = serializers.SerializerMethodField()
    diverifikasi = serializers.SerializerMethodField()
    tagihan = serializers.SerializerMethodField()

    def get_siswa(self, obj):
        if not obj.siswa:
            return
        return obj.siswa.nama_siswa

    def get_diskon(self, obj):
        if not obj.diskon:
            return
        return obj.diskon.nama_diskon

    def get_tagihan(self, obj):
        if not obj.tagihan:
            return
        return obj.tagihan.nama_tagihan

    def get_diverifikasi(self, obj):
        if not obj.diverifikasi:
            return
        return obj.diverifikasi.username

    def get_kelas(self, obj):
        if not obj.kelas:
            return
        return obj.kelas.kelas


class MasterKelasSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.MasterKelas
        fields = '__all__'
