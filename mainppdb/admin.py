from django.contrib import admin

from .pool import admin_model_pool
from . import models
from django.utils.translation import ugettext_lazy as _
from django.utils.html import format_html
from django.db.models import Count
import pandas as pd


class PelunasanInline(admin.TabularInline):
    model = models.PelunasanTagihan
    extra = 1
    mainppdb_description = 'Pelunasan'
    fields = ('tanggal', 'dibayar' ,'siswa', 'tagihan', 'diskon', 'diverifikasi')

    def get_extra (self, request, obj=None, **kwargs):
        if obj:
            return 0
        return self.extra

class SiswaAdmin(admin.ModelAdmin):
    model = models.MasterSiswa
    inlines = (
        PelunasanInline,
    )
    list_display = (
        'nama_siswa', 'asal_sekolah', 'tanggal_daftar',
        'diterima_di_kelas', 'nisn', 'no_telp_ortu',
        'cetak'
    )
    fieldsets = (
        (_('Data Siswa'), {
            'fields': (
                ('nama_siswa',),
                ('asal_sekolah',),
                ('alamat'),
                ('nisn', 'pindahan'),
                ('diterima_di_kelas',)
            )
        }),
        (_('Data Orang Tua'), {
            'fields': (
                ('no_telp_ortu',),
                ('nama_ortu_ayah', 'pekerjaan_ortu_ayah', 'pendidikan_terakhir_ayah', 'penghasilan_ortu_ayah'),
                ('nama_ortu_ibu', 'pekerjaan_ortu_ibu','pendidikan_terakhir_ibu', 'penghasilan_ortu_ibu')
            )
        })
    )
    def cetak(self, obj):
        cetak = format_html("""<a class="button" href="/print_siswa/{}">Cetak</a>""".format(str(obj.id)))
        return cetak

    def change_view(self, req, object_id, form_url='', extra_context=None):
        extra_context = extra_context or {}
        extra_context['cetak'] = True
        return super(SiswaAdmin, self).change_view(
            req, object_id, form_url=form_url, extra_context=extra_context)


class KelasAdmin(admin.ModelAdmin):
    model = models.MasterKelas
    list_display = (
        'kelas',
    )


class TagihanAdmin(admin.ModelAdmin):
    model = models.MasterTagihan
    list_display = (
        'nama_tagihan', 'keterangan', 'biaya', 'kelas'
    )


class StatusSiswaLunasAdmin(admin.ModelAdmin):
    model = models.StatusLunasSiswa
    list_display = (
        'siswa', 'dibayarkan'
    )


class DiskonAdmin(admin.ModelAdmin):
    model = models.MasterDiskon
    list_display = (
        'nama_diskon', 'jumlah', 'tagihan', 'tanggal_awal', 'tanggal_akhir'
    )


# class StatistikPelunasan(admin.ModelAdmin):
#     model = models.PelunasanTagihan
#     change_list_template = 'desktop/change_list_graph.html'
#
#     def has_add_permission(self, request):
#         return False
#
#     def change_view(self, request, extra_context=None):
#         qs = models.PelunasanTagihan.objects.all()
#         master = pd.DataFrame(list(qs, columns=['tanggal','siswa.nama_siswa','tagihan']))
#         response = super().changelist_view(
#             request, extra_context=extra_context,
#         )
#         response.context_data['statistik'] = master.values.tolist()
#         return response


class StatistikPendaftar(admin.ModelAdmin):
    model = models.StatistikPendaftar
    change_list_template = 'desktop/change_list_graph_2.html'

    def has_add_permission(self, request):
        return False

    def changelist_view(self, request, extra_context=None):
        qs = models.StatistikPendaftar.objects.values('asal_sekolah', 'nama_siswa', 'tanggal_daftar').annotate(Jumlah=Count('asal_sekolah'), Daftar=Count('tanggal_daftar')).all()
        master = pd.DataFrame(qs)
        master['tanggal_daftar'] = master['tanggal_daftar'].dt.date
        response = super(StatistikPendaftar, self).changelist_view(
            request, extra_context=extra_context,
        )
        extra_context = extra_context or {}
        extra_context['show_save_and_continue'] = False
        extra_context['show_save'] = False
        response.context_data['statistik'] = master.values.tolist()
        response.context_data['total'] = master['Daftar'].values.tolist()
        # print(response.context_data['statistik'])
        return response


# Register your models here.

# admin.site.register(models.MasterSiswa, SiswaAdmin)
admin_model_pool.register(
    models.MasterSiswa, SiswaAdmin,
    inline_classes=[PelunasanInline, ]
)
admin.site.register(models.MasterKelas, KelasAdmin)
admin.site.register(models.MasterTagihan, TagihanAdmin)

admin.site.register(models.MasterDiskon, DiskonAdmin)

# admin.site.register(models.PelunasanTagihan, PelunasanInline)

# admin.site.register(models.StatusLunasSiswa, StatusSiswaLunasAdmin)
admin.site.register(models.StatistikPendaftar, StatistikPendaftar)
