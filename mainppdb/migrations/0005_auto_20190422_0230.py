# Generated by Django 2.2 on 2019-04-22 02:30

import datetime
from django.db import migrations, models
import django.db.models.deletion
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('mainppdb', '0004_auto_20190415_1047'),
    ]

    operations = [
        migrations.AlterField(
            model_name='masterdiskon',
            name='tanggal_akhir',
            field=models.DateTimeField(default=datetime.datetime(2019, 4, 22, 2, 30, 54, 537835, tzinfo=utc), verbose_name='Tanggal Akhir'),
        ),
        migrations.AlterField(
            model_name='masterdiskon',
            name='tanggal_awal',
            field=models.DateTimeField(default=datetime.datetime(2019, 4, 22, 2, 30, 54, 537808, tzinfo=utc), verbose_name='Tanggal Awal'),
        ),
        migrations.AlterField(
            model_name='pelunasantagihan',
            name='diskon',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='mainppdb.MasterDiskon'),
        ),
        migrations.AlterField(
            model_name='pelunasantagihan',
            name='tanggal',
            field=models.DateTimeField(default=datetime.datetime(2019, 4, 22, 2, 30, 54, 538533, tzinfo=utc), verbose_name='Tanggal'),
        ),
    ]
