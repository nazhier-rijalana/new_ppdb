from rest_framework import (
    viewsets, status, permissions, authentication, exceptions, mixins
)
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.pagination import PageNumberPagination
from rest_framework.decorators import action

from . import serializers
from . import models


class SiswaBaruViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    # authentication_classes = (authentication.SessionAuthentication,)
    serializer_class = serializers.SiswaSerializer
    queryset = models.MasterSiswa.objects.all()

    def get_siswa_instance(self):
        instance = self.get_object()
        instance = models.MasterSiswa.objects.get(id=instance.id)
        return instance

    def create(self, request, *args, **kwargs):
        serializer = serializers.SiswaSerializer(data=request.data)
        print(request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        instance = self.get_siswa_instance()
        serializer = serializers.SiswaSerializer(
            instance=instance, data=request.data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)


class TagihanViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (authentication.SessionAuthentication,)
    serializer_class = serializers.TagihanSerializer
    queryset = models.MasterTagihan.objects.all()

    def get_tagihan_instance(self):
        instance = self.get_object()
        instance = models.MasterTagihan.objects.get(id=instance.id)
        return instance

    def create(self, request, *args, **kwargs):
        serializer = serializers.TagihanSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        instance = self.get_tagihan_instance()
        serializer = serializers.TagihanSerializer(
            instance=instance, data=request.data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class KelasViewSet(viewsets.ModelViewSet):
    queryset = models.MasterKelas.objects.all()
    serializer_class = serializers.MasterKelasSerializer
    permission_classes = (IsAuthenticated,)
    authentication_classes = (authentication.SessionAuthentication,)

    def get_kelas_instance(self):
        instance = self.get_object()
        instance = models.MasterKelas.objects.get(id=instance.id)
        return instance

    def create(self, request, *args, **kwargs):
        serializer = serializers.MasterKelasSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        instance = self.get_kelas_instance()
        serializer = serializers.MasterKelasSerializer(
            instance=instance, data=request.data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class PelunasanTagihanViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (authentication.SessionAuthentication,)
    queryset = models.PelunasanTagihan.objects.all()
    serializer_class = serializers.PelunasanTagihanSerializer

    def get_pelunasan_instance(self):
        instance = self.get_object()
        instance = models.PelunasanTagihan.objects.get(id = instance.id)
        return instance

    def create(self, request, *args, **kwargs):
        serializer = serializers.PelunasanTagihanSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        instance = self.get_pelunasan_instance()
        serializer = serializers.PelunasanTagihanSerializer(
            instance=instance, data=request.data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

class DiskonViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    authentication_classes = (authentication.SessionAuthentication,)
    queryset = models.MasterDiskon.objects.all()
    serializer_class = serializers.DiskonSerializer

    def get_diskon_instance(self):
        instance = self.get_object()
        instance = models.MasterDiskon.objects.get(id = instance.id)
        return instance

    def create(self, request, *args, **kwargs):
        serializer = serializers.DiskonSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        instance = self.get_diskon_instance()
        serializer = serializers.DiskonSerializer(
            instance=instance, data=request.data, partial=True
        )
        serializer.is_valid(raise_exception=True)
        return Response(serializer.data, status=status.HTTP_201_CREATED)
