from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.db.models import Sum
from django.utils import timezone
from djmoney.models.fields import MoneyField
from phone_field import PhoneField


# Create your models here.
class MasterKelas(models.Model):
    class Meta:
        app_label = "mainppdb"
        verbose_name_plural = "Master Kelas"

    kelas = models.CharField("Kelas", blank=True, max_length=255)

    def __str__(self):
        return self.kelas


class MasterSiswa(models.Model):
    class Meta:
        app_label = "mainppdb"
        verbose_name = "Master Siswa"
        verbose_name_plural = "Master Siswa"

    nama_siswa = models.CharField("Nama Siswa", max_length=255,null=True)
    asal_sekolah = models.CharField("Asal Sekolah", max_length=255, null=True)
    tanggal_daftar = models.DateTimeField("Tanggal daftar", auto_now_add=True)
    alamat = models.TextField("Alamat", null=True)
    diterima_di_kelas = models.ForeignKey(MasterKelas, on_delete=None, max_length=255, null=True)
    nisn = models.IntegerField("NISN", blank=True, null=True)
    no_telp_ortu = PhoneField("Nomer Telfon Ortu")
    pindahan = models.BooleanField("Pindahan", default=False)
    nama_ortu_ayah = models.CharField("Nama Ortu Ayah", max_length=255, null=True, blank=True)
    pekerjaan_ortu_ayah = models.CharField("Pekerjaan Ortu Ayah", max_length=255, null=True,blank=True)
    penghasilan_ortu_ayah = models.DecimalField("Penghasilan Ortu Ayah", max_digits=15, null=True, decimal_places=3, blank=True)
    pendidikan_terakhir_ayah = models.CharField("Pendidikan Terakhir Ayah", max_length=255, null=True,blank=True)
    nama_ortu_ibu = models.CharField("Nama Ortu Ibu", max_length=255, null=True,blank=True)
    pekerjaan_ortu_ibu = models.CharField("Pekerjaan Ortu Ibu", max_length=255, null=True,blank=True)
    penghasilan_ortu_ibu = models.DecimalField("Penghasilan Ortu Ibu", max_digits=15, null=True, decimal_places=3,blank=True)
    pendidikan_terakhir_ibu = models.CharField("Pendidikan Terakhir Ibu", max_length=255, null=True,blank=True)

    def __str__(self):
        return self.nama_siswa

    def get_natural_key(self):
        return [self.nama_siswa, ]

    @property
    def getpindahan(self):
        if self.asal_sekolah.startswith("SMA") or self.asal_sekolah.startswith("Sma") or self.asal_sekolah \
                .startswith("sma") or self.asal_sekolah.startswith("ponpes") or self.asal_sekolah.startswith("Ponpes") \
                or self.asal_sekolah.startswith("PP") or self.asal_sekolah.startswith("PONPES") or \
                self.asal_sekolah.startswith("Pondok Pesantren"):
            self.pindahan = True
            return self.pindahan
        else:
            self.pindahan = False
            return self.pindahan


class MasterTagihan(models.Model):
    class Meta:
        app_label = "mainppdb"
        verbose_name_plural = "Master Tagihan"

    nama_tagihan = models.CharField("Nama Tagihan", blank=True, max_length=255)
    keterangan = models.TextField("Keterangan", blank=True)
    biaya = MoneyField(max_digits=14, decimal_places=3, default_currency='IDR')
    kelas = models.ForeignKey(MasterKelas, on_delete=models.CASCADE)

    def __str__(self):
        return self.nama_tagihan


class MasterDiskon(models.Model):
    class Meta:
        app_label = "mainppdb"
        verbose_name = "Master Diskon"
        verbose_name_plural = "Master Diskon"

    nama_diskon = models.CharField("Nama Diskon", blank=True, max_length=255)
    jumlah = models.FloatField("Jumlah", blank=True)
    tagihan = models.ForeignKey(MasterTagihan, on_delete=None, blank=True, null=True)
    tanggal_awal = models.DateTimeField("Tanggal Awal", default=timezone.now())
    tanggal_akhir = models.DateTimeField("Tanggal Akhir", default=timezone.now())

    def __str__(self):
        return self.nama_diskon

    def get_natural_key(self):
        return self.nama_diskon


class PelunasanTagihan(models.Model):
    class Meta:
        app_label = "mainppdb"
        verbose_name = "Pelunasan Siswa"
        verbose_name_plural = "Pelunasan Siswa"

    tanggal = models.DateTimeField("Tanggal", default=timezone.now())
    siswa = models.ForeignKey(MasterSiswa, on_delete=models.SET_NULL, null=True)
    tagihan = models.ForeignKey(MasterTagihan, on_delete=models.CASCADE)
    diverifikasi = models.ForeignKey(User, on_delete=models.CASCADE)
    dibayar = MoneyField(max_digits=14, decimal_places=3, default_currency="IDR", null=True, blank=True)
    diskon = models.ForeignKey(MasterDiskon, on_delete=models.CASCADE, null=True, blank=True)
    is_lunas = models.BooleanField("Lunas ?", default=False)

    def __str__(self):
        return self.siswa.nama_siswa
    # def clean_dibayar(self):
    #     if self.diskon:
    #         print(self.dibayar)
    #         self.dibayar = self.tagihan.biaya - ((self.tagihan.biaya * self.diskon.jumlah) / 100)
    #         print(self.dibayar)
    #     else:
    #         self.dibayar = self.tagihan.biaya
    #
    # def clean(self):
    #     self.clean_dibayar()
    #     self.already_clean = True

    def save(self, *args, **kwargs):
        if self.dibayar and self.tagihan.biaya > self.dibayar:
            self.is_lunas = False

        elif self.dibayar and self.tagihan.biaya == self.dibayar:
            self.is_lunas = True

        elif not self.dibayar:
            self.dibayar = self.tagihan.biaya
            self.is_lunas = True

        if self.diskon and self.is_lunas:
            self.dibayar = self.tagihan.biaya - ((self.tagihan.biaya * self.diskon.jumlah) / 100)
            # self.
        super(PelunasanTagihan, self).save(*args, **kwargs)
        self.already_clean = True


class StatusLunasSiswa(models.Model):
    class Meta:
        app_label = "mainppdb"
        verbose_name = "Status Lunas"
        verbose_name_plural = "Status Lunas"

    siswa = models.OneToOneField(MasterSiswa, on_delete=models.CASCADE)
    dibayarkan = models.ForeignKey(PelunasanTagihan, on_delete=models.CASCADE)

    @property
    def is_diskon(self):
        if self.dibayarkan.diskon is not None:
            return True
        else:
            return False

    @property
    def status(self):
        total = MasterTagihan.objects.values('biaya').aggregate(Sum('biaya'))
        dibayar = PelunasanTagihan.objects.values('biaya').aggregate(Sum('dibayar')).get(
            siswa__nama_siswa=self.siswa.nama_siswa)
        jumlah_total_tagihan = PelunasanTagihan.objects.count()
        jumlah_total_dibayarkan = MasterTagihan.objects.count()
        if jumlah_total_dibayarkan == jumlah_total_tagihan and total == dibayar:
            return "Lunas"
        elif jumlah_total_tagihan < jumlah_total_dibayarkan:
            return "Cicilan"
        else:
            return "Belum Lunas"

    def __str__(self):
        return self.siswa.nama_siswa

    def get_natural_keys(self):
        return self.siswa.nama_siswa


class StatistikLunasSiswa(PelunasanTagihan):
    class Meta:
        proxy = True
        verbose_name_plural = "Statistik Pelunasan"

class StatistikPendaftar(MasterSiswa):
    class Meta:
        proxy = True
        verbose_name_plural = "Statistik Pendaftar"
