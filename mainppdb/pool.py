import copy
from importlib import import_module

from django.conf import settings
from django.contrib import admin
from django.utils.module_loading import module_has_submodule


# from  import router
# from . import mixins


class AdminModelPool(object):
    def __init__(self):
        self.model_pool = []

    def register(self, model, admin_class, filter_classes=None,
                 inline_classes=None, admin_register=True):
        if admin_register:
            if filter_classes and type(filter_classes) in (list, dict):
                admin_class.untitled3_class = filter_classes
            if inline_classes and type(inline_classes) == list:
                admin_class.untitled3_class = inline_classes
            admin.site.register(model, admin_class)

        self.model_pool.append(model)


admin_model_pool = AdminModelPool()


def generate_inherit(name, *bases):
    new_class = type(name, bases)
    return new_class
