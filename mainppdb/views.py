from django.contrib.auth.decorators import login_required
from django.db.models import Sum
from django.shortcuts import render


# Create your views here.
from django.http import HttpResponse
import datetime

from django.views.generic import DetailView

from . import models
from dateutil import tz
from django.db.models import Q

from dateutil import tz


class PrintSiswa(DetailView):
    template_name = "siswa_detail.html"
    model = models.MasterSiswa

    def get_context_data(self, **kwargs):
        context = super(PrintSiswa, self).get_context_data(**kwargs)
        now = datetime.datetime.utcnow()
        HERE = tz.tzlocal()
        UTC = tz.gettz('UTC-7')
        context['siswa'] = models.MasterSiswa.objects.get(id=self.kwargs.get("pk"))
        lunas = models.PelunasanTagihan.objects.filter(siswa_id=self.kwargs.get("pk"), is_lunas=True).all()
        cicilan = models.PelunasanTagihan.objects.filter(siswa_id=self.kwargs.get("pk"), is_lunas=False).all()
        context['tagihan_dilunasi'] = lunas
        context['tagihan_cicilan'] = cicilan
        # print(context['tagihan_dilunasi'])
        gmt = now.replace(tzinfo=UTC)
        context['tagihan_belum'] = models.MasterTagihan.objects.exclude(nama_tagihan__in=lunas.values('tagihan__nama_tagihan')).exclude(nama_tagihan__in=cicilan.values('tagihan__nama_tagihan')).all()
        context['total_dilunasi'] = models.PelunasanTagihan.objects.values('dibayar').filter(siswa_id=self.kwargs.get("pk"), is_lunas=True).aggregate(Sum('dibayar'))
        context['total_cicilan'] = models.PelunasanTagihan.objects.values('dibayar').filter(
            siswa_id=self.kwargs.get("pk"), is_lunas=False).aggregate(Sum('dibayar'))
        # print(context['total_cicilan'])
        context['total_belum'] = models.MasterTagihan.objects.values('biaya').exclude(nama_tagihan__in=lunas.values('tagihan__nama_tagihan')).exclude(nama_tagihan__in=cicilan.values('tagihan__nama_tagihan')).aggregate(Sum('biaya'))
        context['dibayar_tanggal'] = gmt.astimezone(HERE)
        context['verifikasi'] = models.PelunasanTagihan.objects.values('diverifikasi__last_name', 'diverifikasi__first_name').filter(siswa_id=self.kwargs.get("pk")).first()
        return context
