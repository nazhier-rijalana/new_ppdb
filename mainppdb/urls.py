from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.decorators import login_required
from django.urls import path, include

# from .views import test
from . import views
from . import api
from rest_framework.routers import DefaultRouter
router = DefaultRouter(trailing_slash=False)
router.register(r'siswa_baru', api.SiswaBaruViewSet)
router.register(r'tagihan', api.TagihanViewSet)
router.register(r'pelunasan', api.PelunasanTagihanViewSet)
router.register(r'kelas', api.KelasViewSet)
router.register(r'diskon', api.DiskonViewSet)
from django.views.generic.base import RedirectView


urlpatterns = [
    path('api/v1/', include(router.urls)),
    path('api-login/v1/', include('rest_framework.urls')),
    url(r'^print_siswa/(?P<pk>[\w-]+)/$', login_required(views.PrintSiswa.as_view()), name='print'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
